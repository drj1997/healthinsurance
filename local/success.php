<?php

		session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Health insurance</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<?php
		
		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$payid=$_POST["payuMoneyId"];
		$posted_hash=$_POST["hash"];
		$mode = $_POST["mode"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$salt="j1Qx9EjTPQ";

		// Salt should be same Post Request 

		If (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		} 
		else 
		{
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		 
		$hash = hash("sha512", $retHashSeq);
       
		if ($hash != $posted_hash) {
			echo '<script>alert("Transaction Failed")</script>';
		} 
		else 
		{
			
			if(isset($_SESSION["email"]))
			{
				$email = $_SESSION["email"];
		
				require('db_config.php');
				if($mysqli->connect_error){
					die("Connection Error: ". $mysqli->connect_error);
				}
				
				$sql1 = "UPDATE transaction SET status = '$status', payu_id = '$payid' 
																	WHERE tran_id = '$txnid' AND email = '$email'";
				
				if($mysqli->query($sql1)==true)
				{
					
					?>
					
						<div class="container">
							<div class="col-sm-6 col-sm-offset-3 top-buffer">
								<div class="panel panel-default panel">
									<div class="row">
										<label style="margin-left:30px; margin-top:20px;">Transaction Successful</label>
									</div>	
									<div class="row">
										<label style="margin-left:30px; margin-top:20px;">Your Transaction ID for this payment is <?php echo $payid; ?> </label>
									</div>	
									<div class="row">
										<label style="margin-left:30px; margin-top:20px;">We have received a payment of Rs. <?php echo $amount; ?> </label>
									</div>
									<div class="row">
										<label style="margin-left:30px; margin-top:20px; margin-bottom:20px;">Your order will be delivered soon </label>
									</div>	
								</div>
							</div>
						</div>
					
<?php
					
				}
				else
				{
					echo '<script>alert("Transaction Successful but could not update in database")</script>';
				}
				
				mysqli_close($mysqli);
				
			}
		}
		
?>	


</body>
</html>


